function [skelmod] = addpoint( xlist, ylist, skeleton)

s = size(xlist);
s = s(1);

skelmod = skeleton;

for i=1:s
  skelmod(xlist(i), ylist(i)) = 0;
end

for i=1:s
  [xr yr] = around(xlist(i), ylist(i), 1);
  sb = size(xr);
  sb = s(1);
  nbvoisins = 0;
  for j=1:sb
    if skelmod(xr(j), yr(j)) ~= 7
      nbvoisins = nbvoisins + 1;
    end
  end
  if nbvoisins == 1
    skelmod(xlist(i), ylist(i)) = 3;
  else if nbvoisins >= 3
      skelmod(xlist(i), ylist(i)) = 4;
  end
end

for i=1:s
  [xr yr] = around(xlist(i), ylist(i), 1);
  sb = size(xr);
  sb = s(1);
  for j=1:sb
    if skelmod(xr(j), yr(j)) == 4
      skelmod(xlist(i), ylist(i)) = 2;
    end
  end
end
