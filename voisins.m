function [ v ] = voisins(skel, point)

couts = ones(size(skel))*inf;
couts(point(1),point(2)) = 0;

[v, mat] = voisins_aux(couts, skel,couts, point, point, 0, 0);
v(:,4:end)=v(:,4:end)~=inf;

%mat
%imagesc(mat);