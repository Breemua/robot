function [ skel ] = link( x, y, skeleton, scene )

[m, n] = size(scene);

xres = x;
yres = y;
xl = [xres];
yl = [yres];

distance = m;

if skeleton(x,y) == 7
  for i=1:m
    for j=1:n
      
	if skeleton(i,j) ~= 7
	  [cross, xlist, ylist] = crossBorder(x,y,i,j,scene);
	  tmpdist = norm([x y]-[i j]);
	  if ~cross && tmpdist < distance
	    xres = i;
	    yres = j;
	    xl = xlist;
	    yl = ylist;
	    distance = tmpdist;
	  end
	end
      
      
    end
  end
end


skel = skeleton;

s = size(xl);
s = s(1);
for k=1:s
  skel(xl(k), yl(k)) = 0;
end