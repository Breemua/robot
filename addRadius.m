function [ expended ] = addRadius( room, rad )
%ADDRADIUS Summary of this function goes here
%   Detailed explanation goes here
%disp(rad);
if rad>0 
  [m,n] = size(room);
  expendedRoom = zeros(m,n);
  for i=1:m
    for j=1:n
      if room(i, j) == 7;
	expendedRoom(i, j) = 7;
	
	if i+1 <= m && j+1 <= n
	  expendedRoom(i+1, j+1) = 7;
	end
	if i+1 <= m && j-1 > 0
	  expendedRoom(i+1, j-1) = 7;
	end
	if i-1 > 0 && j+1 <= n
	  expendedRoom(i-1, j+1) = 7;
	end
	if i-1 > 0 && j-1 > 0
	  expendedRoom(i-1, j-1) = 7;
	end
	if i-1 > 0
	  expendedRoom(i-1, j) = 7;
	end
	if i+1 <= m
	  expendedRoom(i+1, j) = 7;
	end
	if j-1 > 0
	  expendedRoom(i, j-1) = 7;
	end
	if j+1 <= n
	  expendedRoom(i, j+1) = 7;
	end
	
      end
    end
  end
  expended = addRadius(expendedRoom, rad-1);
else
  expended = room;
end

