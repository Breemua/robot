load('scene_chambres.mat');




%load('labyrinthe.mat');
%imscene=scene;
%im=imscene;

%imscene = addRadius(imscene, 2);

im=imscene;

squel;



x_depart = 5;
y_depart = 5;
depart = [x_depart, y_depart];

x_arrive = 50;
y_arrive = 20;
arrive = [x_arrive, y_arrive];

skel = link(x_depart, y_depart, im, imscene);

skel = link(x_arrive, y_arrive, skel, imscene);

skel = postsquelettisation(skel, depart, arrive);

colormap('default');
%figure
imagesc(skel);

[adjacence, sommets, chemins] = matriceAdjacence(skel);

nbchemins = size(chemins);
nbchemins = nbchemins(1);

% On cherche l'indice du point de départ
ns = size(sommets);
ns = ns(1);
idep=1;
while idep < ns && (sommets(idep, 1) ~= x_depart || ...
		    sommets(idep, 2) ~= y_depart)

  idep = idep+1;
end
if sommets(idep, 1) ~= x_depart || sommets(idep, 2) ~= y_depart
  disp('sommet de depart non trouve');
end


% On cherche l'indice du point d'arrive
iarr=1;
while iarr < ns && (sommets(iarr, 1) ~= x_arrive || ...
		    sommets(iarr, 2) ~= y_arrive)

  iarr = iarr+1;
end
if sommets(iarr, 1) ~= x_arrive || sommets(iarr, 2) ~= y_arrive
  disp('sommet de depart non trouve');
end


[path, totalCost, farthestPreviousHop, farthestNextHop] ...
    = dijkstra(ns, adjacence, idep, iarr, ones(1,ns), ones(1,ns));

idep;
iarr;
path;

nbdij = size(path);
nbdij = nbdij(2);

parcours = [];
for i=1:nbdij-1
  parcours = [parcours ; chemins(path(i), path(i+1),:)];
end

sizep = size(parcours);
sizep = sizep(1);
if sizep>1
  M=reshape(sum(parcours),70,[]);
else
  M=reshape(parcours,70,[]);
end
M=M~=0;
M = int16(M) + int16(imscene);
figure
imagesc(M);