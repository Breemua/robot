function [ skelmod  ] = postsquelettisation(skel, depart, arrive)
skelmod = skel;

[m, n] = size(skelmod);


% On determine les 4-carrefours
for i=1:m
  for j=1:n
    if skelmod(i,j) ~= 7
      [xr yr] = around(i, j);
      nbvoisins = 0;
      tmpx = -1;
      tmpy = -1;
      for k=1:4
        if xr(k) > 0 && xr(k) <= m && yr(k) > 0 && yr(k) <= n
    	  if skelmod(xr(k), yr(k)) ~= 7
            nbvoisins = nbvoisins + 1;
          end
        end
      end
      if nbvoisins >= 3
        skelmod(i, j) = 4;
      end
    end
  end
end


% On determine les autres carrefours
for i=1:m
  for j=1:n
    if skelmod(i,j) ~= 7
      [xr yr] = around(i, j);
      nbvoisins = 0;
      tmpx = -1;
      tmpy = -1;
      voisindevoisin = false;
      for k=1:8
        if xr(k) > 0 && xr(k) <= m && yr(k) > 0 && yr(k) <= n
    	  if skelmod(xr(k), yr(k)) ~= 7
	    nbvoisins = nbvoisins + 1;
	    if skelmod(xr(k), yr(k)) == 4
	      voisindevoisin = true;
	    end
          end
        end
      end
      if nbvoisins >= 3 && ~voisindevoisin
        skelmod(i, j) = 4;
      end
    end
  end
end


% On determine les extremites et les pixels alignés
for i=1:m
  for j=1:n
    if skelmod(i,j) ~= 7
      [xr yr] = around(i, j);
      nbvoisins = 0;
      align = false;
      tmpx = -1;
      tmpy = -1;
      for k=1:8
        if xr(k) > 0 && xr(k) <= m && yr(k) > 0 && yr(k) <= n
	  if skelmod(xr(k), yr(k)) ~= 7
	    nbvoisins = nbvoisins + 1;
	    if tmpx ~= -1 && tmpy ~= -1
	      if xr(k) == 2*i - tmpx && yr(k) == 2*j -tmpy
		align = true;
	      end
	    else
	      tmpx = xr(k);
	      tmpy = yr(k);
	    end
	  end
        end
      end
      if nbvoisins == 1
        skelmod(i, j) = 3;
      elseif nbvoisins == 2
      	  if align
      	    skelmod(i, j) = 0;
      	  else
      	    skelmod(i, j) = 1;
      	  end
      end
    end
  end
end

% On rajoute en tant que noeud les points de depart/arrive
skelmod(depart(1), depart(2)) = 4;
skelmod(arrive(1), arrive(2)) = 4;


% On determine les pixels complexes
for k=1:2
for i=1:m
  for j=1:n
    if skelmod(i,j) ~= 7 && skelmod(i,j) ~= 3 && skelmod(i,j) ~= 4
      [xr yr] = around(i, j);
      sb = size(xr);
      sb = sb(1);
      for k=1:sb
        if xr(k) > 0 && xr(k) <= m && yr(k) > 0 && yr(k) <= n
	  if skelmod(xr(k), yr(k)) == 4
	    skelmod(i,j) = 2;
	  end
        end
      end
    end
  end
end
end

% On determine les sommets simples
for i=1:m
  for j=1:n
    if skelmod(i,j) == 1
      [xr yr] = around(i, j);
      sb = size(xr);
      sb = sb(1);
      for k=1:sb
        if xr(k) > 0 && xr(k) <= m && yr(k) > 0 && yr(k) <= n
	  if skelmod(xr(k), yr(k)) == 0
	    skelmod(i,j) = 5;
	  end
        end
      end
    end
  end
end
 
