function [adjacence, sommets, chemins] = matriceAdjacence( skel )

[m, n] = size(skel);

sommets = [];

for i=1:m
  for j=1:n
    if skel(i,j) == 3 || skel(i,j) == 4
      sommets = [sommets; [i j]];
    end
  end
end

nbsommets = size(sommets);
nbsommets = nbsommets(1);

chemins = zeros(nbsommets,nbsommets, m*n);

adjacence = Inf*ones(nbsommets, nbsommets);



for i=1:nbsommets
  sommet = sommets(i,:);
  v = voisins(skel, sommet);
  s = size(v);
  s = s(1);
  for j=1:s
    k = 1;
    while sommets(k,1) ~= v(j,1) || sommets(k,2) ~= v(j,2)
      k = k + 1;
    end
    if v(j, 3) < adjacence(i, k)
      adjacence(i, k) = v(j, 3);
      adjacence(k, i) = v(j, 3);
      chemins(i,k,1:end)= v(j,4:end);
      chemins(k,i,1:end)= v(j,4:end);
    end
    if v(j, 3) == adjacence(i, k)
      adjacence(i, k) = v(j, 3);
      adjacence(k, i) = v(j, 3);

      prev = chemins(i,k,:);
      prev = squeeze(prev(1,1,:))';

      chemins(i,k,1:end)=prev+ v(j,4:end);
      chemins(k,i,1:end)= prev+v(j,4:end);
    end
  end
end