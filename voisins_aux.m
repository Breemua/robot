function [v, mat] = voisins_aux(couts, skel,chemin, simple_precedent, ...
			   point_courant, distance, cpt)



mat = couts;
[m,n] = size(mat);

v = [];
[xr yr] = around(point_courant(1), point_courant(2));
sb = size(xr);
sb = sb(1);


nodes = [];
notnodes = [];
node = false;
for k=1:sb
  if xr(k) > 0 && xr(k) <= m && yr(k) > 0 && yr(k) <= n
    if skel(xr(k), yr(k)) == 4 && mat(xr(k), yr(k)) ~= 0
      node = true;
      nodes = [nodes; [xr(k), yr(k)]];
    else
      notnodes = [notnodes; [xr(k), yr(k)]];
    end
  end
end

if node
ttn = nodes;
else
ttn = [nodes; notnodes];
end


xr = ttn(:,1);
yr = ttn(:,2);
sb = size(xr,1);

chemin(point_courant(1), point_courant(2)) = 1;

for k=1:sb
  if xr(k) > 0 && xr(k) <= m && yr(k) > 0 && yr(k) <= n


    if mat(xr(k), yr(k)) > cpt && skel(xr(k), yr(k)) ~= 7
      

      if skel(xr(k), yr(k)) == 4 || skel(xr(k), yr(k)) == 3
	% Noeuds, but ultime où l'algo recursif s'arretera
	

	chemin(xr(k), yr(k)) = 1;
	
	mat(xr(k), yr(k)) = mat(point_courant(1), point_courant(2)) + 1;
	cresh = reshape(chemin,1,[]);
	v = [v; xr(k), yr(k), distance+norm([xr(k),yr(k)]-simple_precedent), ...
	     cresh(1,1:m*n)];
	
      elseif skel(xr(k), yr(k)) == 2 || skel(xr(k), yr(k)) == 5
	% Points simples ou complexes, déclanchent le calcul des
	% distances

	  mat(xr(k), yr(k)) = mat(point_courant(1), point_courant(2)) + 1;
	  [vois, mat] = voisins_aux(mat, skel,chemin, ...
			       [xr(k), yr(k)], ...
			       [xr(k), yr(k)], ...
			       distance+norm([xr(k),yr(k)]- ...
					     simple_precedent),cpt+1);
	  v = [v; vois];
	  
      elseif skel(xr(k), yr(k)) == 0 || skel(xr(k), yr(k)) == 1

	mat(xr(k), yr(k)) = mat(point_courant(1), point_courant(2)) + 1;
	[vois, mat] = voisins_aux(mat, skel,chemin,...
			     simple_precedent, ...
			     [xr(k), yr(k)], ...
			     distance, cpt+1);
	v = [v; vois];

      end
   
    end
  end
end

