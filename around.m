function [xr yr] = around(x, y)



    % helps when we want the 4 neighbors of a point
    xr = x + [-1; 0; 0; 1; -1; -1; 1; 1];
    yr = y + [0; -1; 1; 0; -1; 1; -1; 1];
