function [ cross, x, y ] = crossBorder( x1, y1, x2, y2, scene )
%CROSSBORDER Summary of this function goes here
%   Detailed explanation goes here
    
    [x, y] = bresenham(x1, y1, x2, y2);
    s = size(x);
    s = s(1);
    
    i=1;
    cross = false;
    
    while i <= s && ~cross
        if scene(x(i), y(i)) == 7
            cross = true;
        end
	i = i + 1;
    end
end

